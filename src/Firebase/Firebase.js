import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const Config = {
  apiKey: process.env.REACT_APP_APIKEY,
  authDomain: "simple-todos-babdb.firebaseapp.com",
  databaseURL: "https://simple-todos-babdb.firebaseio.com",
  projectId: "simple-todos-babdb",
  storageBucket: "simple-todos-babdb.appspot.com",
  messagingSenderId: "807511019914",
  appId: "1:807511019914:web:b110d4509e382a636cf873"
};

firebase.initializeApp(Config);
firebase.firestore();

export default firebase;
